# What is Basin-Scale Modelling?

## Key Features

Basin-scale modelling is a type of hydrological method known as a "rainfall run-off method". Rainfall run-off methods produce hydrological estimates by simulating the physics of water as it moves through the landscape.

There are four key hydrological processes that should be represented by a basin-scale model:

  * Rainfall: the distribution of water across the catchment by a storm.
 * Overland flow: the run-off of water across the surface of the landscape.
 * Infiltration: the absorption of water by the soil.
 * Sub-surface flow: the routing of the water through the ground.
 
There are also a number of further processes that it may be appropriate to include in a basin-scale model, such as:

  * Interception: the interception of rainfall by vegetation.
  * Evaporation: the loss of water from the soil and the surface due to evaporation.
  * Transpiration: the loss of water from the soil due to its uptake by vegetation.

  > might be good to explain these processes without the process you are trying to explain in the description? Having said that I've tried to think of an alternative for interception and I cannot 

## When is Basin-Scale Modelling Appropriate?

The basin-scale modelling approach can provide extremely accurate hydrological predictions for complex catchments. It is particularly appropriate when an accurate determination of run-off or flood risk is required across a whole basin or catchment, when the run-off has been significantly altered or intercepted by localised man-made or geological features, or when there are complex patterns of rainfall or infiltration distribution.

At the moment, nationally applicable parameters for infiltration and inter-flow are not available and the method therefore has limited utility on ungauged catchments, or catchments that have little or no calibration data available.

## Comparison with the FEH Methods

In order to understand how basin-scale modelling can be applied in the UK it is useful to compare it to the current, widely-used approaches for assessing the run-off and flood hydrology of catchments. The methods currently in wide use in the UK are defined in the Flood Estimation Handbook[^1] with substantial revisions and updates published by the various regulatory agencies operating in the UK[^2].

As a rainfall run-off method, the basin-scale modelling approach is most comparable to the FEH rainfall run-off methods. The FEH rainfall run-off method that is currently in general use is the Revitalised Flood Hydrograph (ReFH) method (version 2.4).

The two methods share a fundamental approach to catchment modelling: a rainfall hyetograph is split into a rapid response (overland flow) component and a delayed response (baseflow, interflow) component. These components are routed through the catchment to the area of interest. There are two fundamental differences between the approaches, however. The basin-scale modelling approach models each hydrological process with a much higher level of detail, both in space and time, and it also models each process with a more complex, physically-based model. This provides much more confidence when extrapolating the results beyond the scope of the models' calibration.

> not quite sure what is meant by 'beyond the scope of the models' calibration' perhaps an example would be good?

The ReFH methods are catchment-averaged approaches in which a single set of parameters is calculated to be representative of the entire catchment. By contrast, basin-scale modelling subdivides the catchment into a very large number of cells, each of which can have individually calculated parameters based on their location in the catchment and the physical characteristics of that location on the ground. This increase in the level of detail gives basin-scale modelling much more descriptive power and a correspondingly greater ability to represent complex hydrological and hydraulic conditions that are "averaged away" by the catchment-averaging process.

As with space, so with time. The basin-scale modelling approach utilises a much smaller "timestep": the interval at which the state of the catchment is recalculated and a point on the output hyetograph can (optionally) be generated. Timesteps for the basin-scale approach may be of the order of one second (depending on the model used) while ReFH timesteps are likely to be 2-3 orders of magnitude longer.

A common criticism of basin-scale modelling is that this increased level of detail comes along with a very large increase in the number of parameters in the model leading to a model that is over-parameterised and hence over-fit. One of the key purposes of this document is to set standards for how catchment data should be interpreted in basin-scale models, effectively reducing the number of parameters from "per-cell" to "per-feature". By using datasets that are available on a national scale this approach also aims, in the future, to give parameter estimates that are valid nationally, reducing (but not eliminating) the reliance on individual model calibration.

> personally I would get rid of the above three paragraphs because there is a lot of detail that perhaps is overkill for an overview? It also means there is a long time to get to the second significant difference. Perhaps if the above three paragraphs want to be maintained there would be scope to provide this additional detail after outlining the second significant difference?

The second significant difference between the basin-scale and ReFH approaches is in the nature of the process models used. The basin-scale approach uses the Green & Ampt infiltration functions on a per-cell, per-timestep basis. This allows spatially and temporally varying infiltration rates that are dynamically calculated from local surface and groundwater depths. By contrast the ReFH method, with a percentage run-off based infiltration scheme, is based on a single, catchment-wide soil moisture parameter which cannot capture several important aspects of the physical process of infiltration. Such as reduced infiltration on steep slopes (due to rapid run-off limiting the available surface water) or increased pressure-driven infiltration under inundated areas.


[^1]: [Flood Estimation Handbook](https://www.ceh.ac.uk/services/flood-estimation-handbook) on CEH website. Retrieved 30th August 2022.

[^2]: e.g. [LIT11832](https://www.gov.uk/government/publications/river-modelling-technical-standards-and-assessment/estimate-flood-flow-from-rainfall-and-river-flow-data-source), covering England, on gov.uk website. Retrieved 30th August 2022. 
