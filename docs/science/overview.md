# Overview

The following sections provide additional scientific background to support the assertions and recommendations made in this guidance document. It is not necessary for most practitioners of basin-scale modelling to be familiar with the details and concepts contained within these sections, but they are included to provide scientific background to those practitioners who are interested in fully understanding their work and to offer justifications for some of the choices of recommendations made in the preceding sections. This part of the guidance also aims to highlight ongoing areas of scientific uncertainty where either the basin-scale approach, or our understanding of the hydrological processes modelled are uncertain or in need of further research and refinement.

