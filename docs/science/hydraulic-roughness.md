# Hydraulic Roughness

## Suggested Manning's _n_ values

#### Urban Environments

#### Agricultural Environments

Manning's _n_ values for various crops:

| Land Use                                       |           | _n_ min | _n_ average | _n_ high |
|------------------------------------------------|-----------|---------|-------------|----------|
| Bermuda grass (sparse to good cover) by height | < 50mm    | 0.015   | 0.023       | 0.04     |
|                                                | 50-100mm  | 0.030   | 0.046       | 0.060    |
|                                                | 150-200mm | 0.030   | 0.074       | 0.085    |
|                                                | 250-600mm | 0.040   | 0.100       | 0.150    |
|                                                | > 600mm   | 0.060   | 0.150       | 0.200    |
| Bermuda grass (dense cover)                    |           | 0.300   | 0.410       | 0.480    |
| Other dense sod-forming grasses                |           | 0.390   | 0.450       | 0.630    |
| Dense bunch grasses                            |           |         | 0.150       |          |
| Annual grasses                                 |           |         | 0.200       |          |
| Kudzu                                          |           | 0.070   | 0.150       | 0.230    |
| Lespedeza (legumes)                            |           |         | 0.100       |          |
| Natural rangeland                              |           | 0.100   | 0.130       | 0.320    |
| Clipped range                                  |           | 0.020   | 0.150       | 0.240    |
| Wheat straw mulch by yield                     | 2.5 t/ha  | 0.050   | 0.055       | 0.080    |
|                                                | 5.0 t/ha  | 0.075   | 0.100       | 0.150    |
|                                                | 7.5 t/ha  | 0.100   | 0.150       | 0.200    |
|                                                | 10.0 t/ha | 0.130   | 0.180       | 0.250    |
| Chopped maize stalks by yield                  | 2.5 t/ha  | 0.012   | 0.020       | 0.050    |
|                                                | 5.0 t/ha  | 0.020   | 0.040       | 0.075    |
|                                                | 10.0 t/ha | 0.023   | 0.070       | 0.130    |
| Cotton                                         |           | 0.070   | 0.080       | 0.090    |
| Wheat                                          |           | 0.100   | 0.125       | 0.300    |
| Sorghum                                        |           | 0.040   | 0.090       | 0.110    |

Manning's _n_ values for bare soils and ploughed fields:

| Land Use                      |              | _n_ min | _n_ average | _n_ high |
|-------------------------------|--------------|---------|-------------|----------|
| Bare soil by roughness depth  | < 25mm       | 0.010   | 0.020       | 0.030    |
|                               | 25-50mm      | 0.014   | 0.025       | 0.033    |
|                               | 50-100mm     | 0.023   | 0.030       | 0.038    |
|                               | > 100mm      | 0.045   | 0.047       | 0.049    |
| Mouldboard plough             |              | 0.020   | 0.060       | 0.100    |
| Chisel plough by residue rate | < 0.6 t/ha   | 0.010   | 0.070       | 0.170    |
|                               | 0.6-2.5 t/ha | 0.070   | 0.180       | 0.340    |
|                               | 2.5-7.5 t/ha | 0.190   | 0.300       | 0.470    |
|                               | > 7.5 t/ha   | 0.340   | 0.400       | 0.460    |
| Disc/harrow by residue rate   | < 0.6 t/ha   | 0.010   | 0.080       | 0.410    |
|                               | 0.6-2.5 t/ha | 0.100   | 0.160       | 0.250    |
|                               | 2.5-7.5 t/ha | 0.140   | 0.250       | 0.530    |
|                               | > 7.5 t/ha   |         | 0.300       |          |
| No tillage by residue rate    | < 0.6 t/ha   | 0.030   | 0.040       | 0.070    |
|                               | 0.6-2.5 t/ha | 0.010   | 0.070       | 0.130    |
|                               | 2.5-7.5 t/ha | 0.160   | 0.300       | 0.470    |
| Coulter                       |              | 0.050   | 0.100       | 0.130    |


