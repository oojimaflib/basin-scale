# Defining Surface Conveyance

## Overview

In flood events it is expected that water will flow across the surface of the landscape and the speed, direction and depth that that overland flow assumes will be controlled by the nature of the land surface over which it flows. The property which is most commonly used to define this in hydraulic modelling is the hydraulic roughness, which is most commonly parameterised using Manning's _n_. Unless good local information to inform the selection of Manning's _n_ values is available, we recommend selecting these values based on land-use mapping, with pre-defined values for each categorised land use.

The other key parameter that should be determined based on the land use maps is the surface perviousness. Soils mapping and data will be used to model the infiltration of water into the ground, but the ability of the overland flow to access that ground (due to the presence of hardstanding, buildings or other man-made structures, for example) is generally not related to the surface geology but instead related to land use. The surface perviousness is generally defined as a fraction representing the proportion of area where the overland run-off has free access to the underlying soils.

While in most cases the land use will simply allow the selection of these two parameters, in some special cases the land use will also imply changes to other parameters and modelling approaches. Examples of this include heathland, where the land-use implies a high water table and woodland, where changes in the soil matrix and hence the hydraulic conductivity might be expected due to the action of the roots. These special cases are discussed in the [special topics](build-special-topics.md) section.

## Land Use Categories

For basin-scale modelling, we recommend that the land is classified into the following categories:

| Name              | Description                                                             |
|-------------------|-------------------------------------------------------------------------|
| Bare Ground       | Relatively smooth, bare soil. Low roughness, pervious.                  |
| Building          | Buildings. Very high roughness, impervious.                             |
| Conifer           | Coniferous woodland. High roughness, pervious.                          |
| Deciduous         | Deciduous woodland. High roughness, pervious.                           |
| Dense Vegetation  | Thick, vegetated waste ground. Very high roughness, pervious.           |
| Field             | Arable fields. Moderate roughness, pervious.                            |
| Hardstanding      | Roads, asphalt, concrete. Low roughness, impervious.                    |
| Heath             | Blanket heathland. Very high roughness, pervious.                       |
| Marsh             | Marsh ground. High roughness, pervious.                                 |
| Rail              | Railways. High roughness, pervious                                      |
| Rough Grassland   | Infrequently grazed pasture and waste ground. High roughness, pervious. |
| Rough Hard Ground | Rough ground with little vegetation. High roughness, pervious.          |
| Water             | Areas covered by water. Low roughness, pervious.                        |

The process by which this classification is achieved is dependent on the input data, but recommendations are made for delineating land use regions from several data sources. Recommendations are made below on the selection of roughness and perviousness values for each of these categories.

## Manning's _n_ Values

Manning's _n_ has several drawbacks. It defines the effects of the land surface at a low level and it will therefore vary not only spatially, with different land uses, but also over time and with different flow conditions. The parameter was intended for use for flow in channels and pipes and is not ideal for parameterising overland flow. Most problematically, the amount of guidance available to the modeller on reasonable _n_ values for different land uses under different conditions is very limited and there has been comparatively little research interest in this area. As a result most of the values available are of uncertain provenance and most values used in practice are derived from long-standing use rather than any rigorous well-researched source.

### Variation with Flow Depth

It is well known that the hydraulic roughness of a vegetated channel will vary according to the relative heights of the vegetation and the flow. The effect of this on overland flows is that the Manning's _n_ parameter will vary with the depth of the overland flow. _n_ will vary from a high value (very rough) when the depth of flow is less than the height of the vegetation and the vegetation therefore causes a blockage throughout the height of the flow; to a lower value when the depth of flow greatly exceeds the height of the vegetation and the vegetation can therefore be thought of more as a surface effect.

Values of Manning's _n_ generally quoted in the literature, for open channels are generally assumed to refer to the lower of these two values as they are generally focussed on the Manning's _n_ value for a flowing channel. The generally accepted rule-of-thumb is that Manning's _n_ values for shallow flows are ten times higher than those for deep flows across comparable vegetation. A full discussion on Manning's _n_'s variation with the nature of the flow is provided [here](../science/depth-varying-mannings.md).

### Suggested Values

The following table gives recommendations for values of Manning's _n_ for shallow (\(n_0\)) and deep (\(n_1\)) flow as well as the range of depths (\(d_0-d_1\)) where _n_ should transition between the two values.

| Land Use          | \(n_0\) | \(n_1\) | \(d_0\) | \(d_1\) |
|-------------------|---------|---------|---------|---------|
| Bare Ground       | 0.200   | 0.020   | 0.025   | 0.050   |
| Building          | 0.020   | 1.000   | 0.050   | 0.100   |
| Conifer           | 0.300   | 0.06    | 0.100   | 0.200   |
| Deciduous         | 0.300   | 0.08    | 0.100   | 0.200   |
| Dense Vegetation  | 0.100   | 0.050   | 1.000   | 1.500   |
| Field             | 0.130   | 0.050   | 0.200   | 0.300   |
| Hardstanding      | 0.100   | 0.020   | 0.01    | 0.025   |
| Heath             | 0.300   | 0.100   | 0.100   | 0.200   |
| Marsh             | 0.300   | 0.100   | 0.100   | 0.200   |
| Rail              | 0.300   | 0.080   | 0.100   | 0.200   |
| Rough Grassland   | 1.000   | 0.100   | 0.100   | 0.200   |
| Rough Hard Ground | 0.300   | 0.050   | 0.050   | 0.100   |
| Water             | 0.030   | 0.030   | N/A     | N/A     |

### Notes on Particular Land Uses

#### Bare Ground

The \(n_1\) value for bare ground is taken from Morgan _et. al._ (1998)[^1] and is increased by a factor of ten for shallow flows. The depth of the roughness features is assumed to be less than 2.5 cm, giving a transition region between 2.5-5.0 cm depth. The modeller might choose to adjust the _n_ values by a factor for known areas of rougher ground and increase the transition range accordingly. Guidance on _n_ values for bare soil with various roughness depths is given [here](../science/hydraulic-roughness.md).

#### Building

For buildings the usual order of the Manning's _n_ parameters is reversed. Shallow flows on buildings in basin-scale models are generally generated through rainfall which, in reality, would hit a roof and run off rapidly, yielding the low value of \(n_0\). When flows are deep these are representative of overland flood flows that would be significantly slowed by the presence of a building, yielding a high \(n_1\) value. The transition depth is somewhat arbitrary, chosen to be close to but above the highest depth that might occur purely due to rainfall. The modeller might choose to alter this depth for particularly low- or high-intensity storms.

#### Conifer and Deciduous Woodland

The Manning's _n_ of woodland is highly uncertain. It is assumed that both coniferous and deciduous woodland will behave similarly, with deciduous woodland having a slightly higher roughness due to having rougher ground and more underbrush. This reflects, in part, the fact that much of the coniferous woodland in the UK is extensively managed.

#### Field

This category, intended to cover arable fields, is very uncertain as the roughness of arable fields varies markedly through the year and any general roughness recommendation will therefore necessarily be poor. The \(n_0\) value provided falls close to the average _n_ value for a range of crops and tillage practices provided by Morgan _et. al._ (1998)[^1] and a fairly arbitrary transition depth of 200-300 mm is used to transition to a lower value, more consistent with long-running practice in flood modelling.

#### Hardstanding

Intended to cover roads, car parks, pavements and other areas of impermeable, paved ground. The _n_ values and transition depths are both very low, consistent with long-standing practice.

#### Heath

#### Marsh

#### Rail

#### Rough Grassland

#### Rough Hard Ground

#### Water


## Surface Perviousness Values

As with Manning's _n_ values, there is comparatively little research that might allow a modeller to make a truly informed decision about the selection of surface perviousness. The selection of perviousness values must therefore necessarily be a "common-sense" approach, depending on the data available, the level of detail of the modelling and the specific hydrological features and processes that the surface perviousness might represent.

Our suggested values, below, are binary--either a land use class is fully pervious or fully impervious, but a modeller might choose to adjust the values away from either 1.0 or 0.0 to represent features such as soil compaction or to represent areas with both hardstanding and open soil at a coarser level of detail.

### Suggested Values

The following table gives our recommended surface pervious values for each of the recommended land use categories.

| Land Use          | Pervious Fraction | Impervious Fraction |
|-------------------|-------------------|---------------------|
| Bare Ground       | 1.0               | 0.0                 |
| Building          | 0.0               | 1.0                 |
| Conifer           | 1.0               | 0.0                 |
| Deciduous         | 1.0               | 0.0                 |
| Dense Vegetation  | 1.0               | 0.0                 |
| Field             | 1.0               | 0.0                 |
| Hardstanding      | 0.0               | 1.0                 |
| Heath             | 1.0               | 0.0                 |
| Marsh             | 1.0               | 0.0                 |
| Rail              | 0.0               | 1.0                 |
| Rough Grassland   | 1.0               | 0.0                 |
| Rough Hard Ground | 0.0               | 1.0                 |
| Water             | 0.0               | 1.0                 |



[^1]: [The European Soil Erosion Model (EUROSEM) documentation and user guide; Morgan et al (1998)](https://eprints.lancs.ac.uk/id/eprint/13189/1/user_v2.pdf)
