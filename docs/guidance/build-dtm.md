# Ground Surface Model

## General Comments

The basin-scale modelling approach requires a high-quality representation of the ground surface in order to model the flow of water across that surface. This model must generally be built up, by the modeller from a number of data sources, most of which will require manual corrections and enhancements to ensure that the model's interpretation of the terrain conforms to what is expected by a modeller.

## The Terrain Model

The main source of data for the ground surface model will be a broad-scale digital terrain model (DTM), probably derived from LiDAR. These models are generally available as raster images with a pixel resolution of between 0.5 and 2 metres. For a basin-scale model, the DTM will frequently require human review and correction to ensure that flow routes are correctly represented, particularly at bridges and in urban areas. Where the modelling software allows this, we would recommend making these changes using the modelling software rather than adjusting the input DTM grids.

### Using Multiple Terrain Models

Oftentimes a catchment is not fully covered by a single terrain model. It is common, for example, to find that, say 90% of a catchment is covered by high-resolution LiDAR data, but a small area near the upstream extents has no LiDAR available and photogrammetry must be substituted. Great care must be taken when using multiple terrain data sources in this way to avoid step changes in the terrain that could unrealistically impound water.

## Channels

Generally, the most important part of the ground surface model will be the representation of the river channels as this is where the bulk of the overland flow will be moving. These are also the areas where the broad-scale digital terrain models are likely to be least accurate, as LiDAR is generally not capable of producing ground surface elevations from below the water-line and watercourses are often surrounded by trees and substantial vegetation which can also render LiDAR ineffective. There are several options for modelling channels, outlined below. A single model may choose to use different options for different channels depending on their size and location within the model.

### Do Nothing

For very small channels and channels whose flow does not have an appreciable impact on the area of interest it may be appropriate to "do nothing" and leave the channels as they are represented by the digital terrain model. This approach will rarely be appropriate for anything larger than a field drain and, even when this approach is taken, the DTM should be reviewed to ensure that the route of the channel is free of obstructions caused by poor filtering of vegetation or poor triangulation of filtered areas.

Channels represented in this way will generally yield run-off hydrographs that are more attenuated than is realistic and it may be appropriate to give these channels special treatment in the land-use and sub-surface components of the model, even if they are not directly represented in the ground surface model.

### Simplified Representative Channel

It is common for small channels, especially in the upstream catchment to potentially have a significant effect on the flows at the area of interest, but for there to be little to no information available to inform a detailed model of these channels. It is often not feasible to commission survey of these channels and therefore a simplified representation of them must be found that preserves a continuous flow route and provides a reasonable estimate of their size and conveyance capacity.

We recommend the use of _regime equations_ to define a representative rectangular channel in the absence of verifiable on-site data about the channel's geometry. The choice of regime equation to apply is a matter for the user, but a our recommended approach, based on the equations of Parker et al. (2007) is:

  * Define the channels to be represented by digitising channel centre-lines. These centre-lines should be split into segments at tributary/distributary points but also at regular intervals along their length such that no digitised centre-line segment is longer than about 500 m. 
  * Determine, for each centre-line segment the average breadth of the channel, B, by inspection of mapping or aerial photography
  * Determine, for each centre-line segment, the average slope of the landscape by inspection of the base DTM.
  * Assign each centre-line segment an estimated bank-full discharge based on the formula 
  $$
  Q = 0.0658 \sqrt{g} B^{2.5} S^{0.484}
  $$
  * Review the channels to ensure the discharge estimates are consistent with each other and adjust the values accordingly
  * Calculate an estimated channel depth for each centre-line segment from
  $$
  D = 0.129 B S^{0.1936}
  $$
  * Calculate an elevation of the channel bed at each of the nodes between the centre-line segments based on these depths
  * Review the calculated elevations to ensure the elevations generally fall as they progress downstream.
  * Adjust the ground surface model by carving into it a rectangular cross-sectioned channel with these dimensions.

An in-depth discussion of the use of regime equations to define representative channel geometries is presented [here](../science/regime-equations.md).

### Detailed Channel Modelling

For large watercourses and channels whose conveyance properties are critical to the analysis detailed channel modelling is recommended if sufficient topographic survey data can be obtained. This approach involves the construction of a detailed three-dimensional model of the channel bed and banks that can then be merged with the DTM. The construction of this 3D model is beyond the scope of this guide, but in most cases this model will be generated from a combination of surveyed channel cross sections and a surveyed channel centre-line or thalweg line.

This approach leads to a representation of the channel which is typically at a much higher level of detail than the representation of the surrounding floodplains and land surfaces. It is therefore recommended to use grid refinement to increase the level of detail in the model locally to the channel, without increasing model detail across the wider land surface where the resolution of the surface model is poorer. In general, when using detailed channel modelling, there should be at least five model cells across the width of the channel and this level of detail should extend for at least one channel width on either side of the channel.

### One-Dimensional Representation

A common approach in recent hydraulic models has been to use a linked 1D-2D approach, where a traditional 1D solver such as Flood Modeller or ESTRY is used for in-bank flow and this is dynamically linked at bank-top with a 2D solver such as TUFLOW. The configuration of models of this type is beyond the scope of this guide, but it is worth considering the pros and cons of this widely-used approach for basin-scale modelling.

The clear benefit of this approach is that models of this type already exist and they (or portions of them) can be substantially re-used in the construction of a basin-scale model of the same area. This re-use obviously reduces the cost of model development and can mean that some model features have already been calibrated and a head-start can therefore be gained on the model calibration process. It should be noted, however, that such calibration can generally only be relied upon for in-bank flows and that some of the fundamental differences between "standard" 1D-2D modelling and basin-scale modelling will generally mean that out-of-bank calibration will be lost.

There are several significant disadvantages to the use of linked 1D-2D models in basin-scale models. In particular, the use of a different model means that critical features such as infiltration and depth-varying roughness often cannot be used for the in-bank portion of the flow or cannot be applied consistently between the in-bank and out-of-bank flows. In our experience the use of multiple linked models can also lead to technical, or numerical stability problems which are avoided if a single model is used throughout.

In general we do not recommend the use of a one-dimensional representation for in-bank flows for basin-scale models as better results can generally be obtained from a fully two-dimensional model and if a one-dimensional model is available there is generally sufficient information to adopt the detailed channel modelling approach, above.

## Reviewing the Terrain Model

The creation of a good ground surface model is often an iterative process and it is one of the most important processes to get right in a basin-scale model. Once a base terrain model has been assembled (potentially from multiple sources) it should be reviewed to ensure that

  * Significant barriers to flow are present within the model
  * Flow routes (including sub-surface flow routes) through those barriers are represented in the model
  * There are no unrealistic step-changes in the topography due to the use of multiple data sources
  
This process can be done by inspection, but it is often also useful to use the hydraulic model to perform a "frozen catchment" simulation and inspect the results.

Some common (and uncommon) features to look for include:

  * "Holes" or "spikes" in the DTM due to bad data. These will lead to the generation of high depths or high velocities (respectively) which will significantly reduce model stability and increase run-times.
  * Obstructions to flow. These might be man-made (such as road or rail embankments) or naturally-occuring (glacial morraine features, for example). Often these will need to be explicitly represented in the modelling software so that the true crest of the feature is not lost.
  * Bridges and underpasses. Generally, man-made flow barriers such as road and rail embankments (and cuttings) will feature routes by which people and things (including water) can travel from one side to the other. Often, these routes will have been identified by the maker of the original DTM and included, but it is also common for smaller routes and routes that are away from major watercourses to be blocked in the DTM.
  * Missing channels. Not all channels and watercourses are well mapped. The results of a frozen catchment simulation will often highlight the existence of channels, field drains and culverts by showing significant shallow flooding in a frequent event.
  * Quarries. Heavily modified landscapes such as quarries can lead to the formation of deep pools of water in the frozen catchment simulation. It is often necessary to establish where these features drain to and either include that flow route or, (if the feature drains to a different catchment, for example) introduce a boundary condition to prevent a deep pond of water from accumulating.
