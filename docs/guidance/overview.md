# Overview

The following sections lay out the key considerations for building a basin-scale model in general terms. There are corresponding sections that provide specific guidance for implementing each aspect of the basin-scale model in each specific software package that is supported by the method (currently only the TUFLOW software package implementation guidance has been outlined).

## Scoping the Model

The most important task in the creation of any model is to correctly determine the scope of work. Before starting to model you should clearly understand:

 * [The purpose of the model](scope-purpose.md)
 * [The desired outputs of the model](scope-outputs.md)
 * [The available input data](scope-data.md)
 * [The calibration process](scope-calibration.md)

## Building the Model

With the model's scope well understood, the model build can begin. This will involve collating the input data and building, in order, each component of the basin-scale model:

  * [Defining the domain](build-domain.md)
  * [Rainfall inputs](build-rainfall.md)
  * [Terrain model](build-dtm.md)
  * [Measurement locations](build-measurement.md)
  * [Land use mapping](build-landuse.md)
  * [Soils mapping](build-soils.md)
  * [Boundary conditions](build-boundaries.md)
  * [Initial conditions](build-ics.md)
  * [Special topics](build-special-topics.md)

  > should baseflow have a place here?

  > under special topics I'd be thinking about; building representation, DTM refinements, site specific data, drainage networks (i'm aware this is 1d but it's certainly a general consideration that needs to be had)

## Calibrating the Model

With the model built, it must be calibrated to the available data.

  * [Comparing discharge](calibrating-discharge.md)
  * [Comparing percentage run-off](calibrating-pr.md)
  * [Calibrating roughness parameters](calibrating-roughness.md)
  * [Calibrating soil parameters](calibrating-soils.md)
  
## Using the Model

Finally, the model can be used for its intended purpose.

  * [Sensitivity analysis](modelling-sensitivity.md)
  * [Modelling topographic change](modelling-z-change.md)
  * [Modelling land-use change](modelling-land-use-change.md)
  * [Modelling climate change](modelling-climate-change.md)

> should there be a section on viewing and interpreting results? 
