# Managing the Input Data

Basin-scale modelling requires a range of input data to accurately represent a catchment. This section provides a brief summary of the recommended input data sources as well as recommendations for how these should be stored and recorded alongside a model.

## Storing and Recording Model Data

For a model to be considered reliable and useful for the long-term it is necessary to know what data has been included in it and what the provenance of that data was. We therefore recommend that, when working with basin-scale models, a directory/computer folder is maintained containing the original input data for the model, before any processing. We recommend that modellers use the following folder structure:

  * `raw-data`
    * `rainfall`
    * `terrain`
	* `land-use`
	* `soils`
	* `calibration`
  * `processing`
    * ... generic processed data ...
  * ... model files ...

> I can see how baseflow isn't 'raw' data, but does it have a place somewhere? or are we ignoring it because one day TUFLOW will be able to generate it?

The `raw-data` folder should contain the data files that inform the modelling in their raw[^1] form; i.e. exactly as received and without any additional processing. These files should be sub-divided according to broad categories as suggested above.

[^1]: The raw form of the data is not necessarily the form of data directly measured by a sensor, but the form of the data supplied to the modeller. For example "raw" LiDAR data for this purpose might be a composite, filtered raster image; "raw" flow data might be rated from recorded water levels that were themselves converted from electrical signals from a sensor.

A record should be kept of the origin of each of the raw data files. This record should contain, at least, the source of the data (for example the website from which it was downloaded, the publication from which it was taken, or the chain of correspondence leading to its supply), and the date of its presumed validity (generally the date on which it was supplied or obtained unless other information is available).

Alongside the `raw-data` folder, we recommend the inclusion of a `processing` folder. The purpose of this location is to store intermediate, processed data that is derived from the raw data, and which may be relevant to the analysis and reporting of the basin-scale model, but which may not be directly used by the modelling software itself. An example of this would be the various stages of catchment delineation leading to the final model domain.

## Rainfall Data

A basin-scale model will generally be created and run with observed rainfall data and the following sub-sections give a non-exhaustive list of common sources of this data in the UK, with recommendations on its processing and use.

### 15-minute Tipping-Bucket Rain Gauge Data

The UK has an extensive hydrometric rain gauge network under the control of the nations' environmental agencies. This data can generally be requested under the Environmental Information Regulations and used in basin-scale models. The data is in the form of 15-minute accumulations: an evenly-spaced time series of values with each value representing the total depth (volume per unit area) of rainfall since the previous value. This is one of the most readily available sources of rainfall data in the UK and one which requires the least additional processing and it is therefore the generally recommended data source for basin-scale modelling.

> TODO: selection of rain gauges

### Time-of-Tip Rain Gauge Data

The 15-minute data discussed above has been processed from this dataset which is also generally available for UK rain gauges. This data is in the form of an irregular time series of accumulations, generally all of the same depth (volume per unit area). This data can be useful if it is felt that, due to the smallness of the model catchment or the particular hydrological processes at issue, a 15-minute time series of rainfall is too coarse to capture the hydrological response.

### NASA Rainfall Estimates

Rainfall estimates are available at half-hourly intervals from NASA's Global Precipitation Measurement Mission (GPMM, 2014-) and Tropical Rainfall Measurement Mission (TRMM, 1997-2015) between latitudes of ±65°, covering nearly all of the UK. The dataset is coarsely gridded and more difficult to use, but may be suitable for sites where little or no directly-measured rain gauge data is available. The data should be considered much more uncertain than tipping-bucket rain gauge data and consideration should be given to rescaling the data using local storage gauge data on a daily or monthly basis.

## Calibration Data

Calibration data for basin-scale models will generally take the form of observed flows or water levels at a fluvial gauging site. As with the rain gauge data, this is generally available from the nations' environmental agencies and can be requested under the Environmental Information Regulations. The data is generally at a 15-minute temporal resolution. Consideration should be given to the quality of calibration data available and whether it is suitable for use, this is outlined in the [calibration process](scope-calibration.md) section.

## Terrain Data

A detailed and accurate digital terrain model (DTM) is one of the most important components of the basin-scale modelling approach. The most common sources of terrain data in the UK are given below.

### Terrain, Surface and Elevation

A digital terrain model (DTM) is intended to be a "bare-earth" representation of the landscape, with vegetation, buildings and many man-made structures (such as bridge-decks across watercourses) removed. It is generally derived from a digital surface model (DSM) which includes these features and will indicate, for example, the elevation of building rooves, bridge decks and tree canopies. The term "digital elevation model" (DEM) seems to encompass both definitions.

### Composite LiDAR from Nations' Environmental Agencies

England and Wales have high quality LiDAR data covering a large proportion of their areas (>93% for [England](https://environment.data.gov.uk/DefraDataDownload/?Mode=survey), >70% for [Wales](https://lle.gov.wales/catalogue/item/LidarCompositeDataset/?lang=en)) and it is frequently the case that most (if not all) of the catchment to be studied is covered by these datasets when working within England and Wales. If this data is available, it is the generally recommended data-source for basin-scale modelling.

### High-resolution Photogrammetry

The entire UK is covered by a photogrammetry-derived DTM at a resolution of 5 m. The accuracy of this DTM is much poorer than the LiDAR data, but it is generally the best available data source in the absence of LiDAR.

## Land Use Data

Land use mapping is crucial both to the routing of overland flows as well as to the determination of infiltration rates. As such, good quality land use data is a requirement for basin-scale modelling. It is possible to use some global datasets to inform land use such as the CORINE land cover maps and the widely available satellite photography from the likes of Google and Microsoft. In the UK we are fortunate to have high-quality maps available from the Ordnance Survey which can be used to inform land use determinations and the two principal sources of this data are discussed below. Other countries will obviously have their own sources of detailed mapping.

### MasterMap Topography Layer

This data source provides detailed mapping of the entire UK and can be used to inform both hydraulic roughness and surface permeability. Each GIS object in the layer has an associated "feature code" between 10021 and 10213 which can be used as a simple land use classification. The data is relatively expensive, but is often available for work for or with government agencies.


### Open Data Layers

This data can be slightly harder to use than the MasterMap topographic layers but it does contain sufficient information to make a reasonable land use classification at a broad scale and is available at no charge.

	
## Soils Data



