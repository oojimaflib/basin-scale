# Defining the Domain

## General Comments

The _domain_ of a basin-scale model is the land area that is included within the model. This should, in general, extend upstream of the area of interest to encompass the whole catchment and should extend downstream a sufficient distance to ensure that:

 * water levels within the area of interest are not affected by backwater effects from the edge of the model domain; and that 
 * all in-channel and floodplain features whose backwater effects reach the are of interest are included within the model.
 
The domain of a basin-scale model is generally _buffered_, to ensure that the model includes a greater area than required. This means that small inaccuracies in the definition of the watershed, for example, do not affect the model results.

## Defining the Downstream Extent

The process of defining the model domain should start with the selection of the downstream location. This should obviously be downstream of the area of interest and should also be downstream of any in-channel or floodplain structure whose effects, in flood, could extend upstream as far as the area of interest.

The selection of the downstream location is an inherently subjective choice. The further downstream from the area of interest the location is chosen, the more likely the model is to have an accurate representation of all of the hydrological processes that could affect the area. These considerations must be weighed against the time, budget and data requirements within which the modelling is being conducted and a balance must be struck between model completeness and feasibility.

One factor that will often drive the selection of a downstream location for the model is the presence of a gauge. It is almost always worth extending a basin-scale model downstream to include a reliable source of calibration data.

## Defining the Catchment Boundary

Once the downstream boundary has been selected, the catchment upstream of this boundary must be delineated. This catchment area will form the basis of the model domain. 

The _catchment_ upstream of a point is the area within which water could drain past that point. A catchment, for basin-scale modelling purposes, might be significantly larger than an equivalent one for other purposes. A basin-scale modelling catchment should include the "natural catchment"; i.e. the area from which surface run-off would drain past the point via gravity; but it should also include a number of additional areas, if present:

  * Adjacent upstream catchments that might pass flows to or from the natural catchment in extreme flood situations.
  * Areas which are connected to the natural catchment by human interventions such as field drains or diverted watercourses.
  * Areas which are connected to the natural catchment by surface water drainage networks.
  * Areas which contribute flows to the catchment through sub-surface processes.
  
It is recommended that the catchment boundary is delineated in a variety of ways and the results of each of those delineations are combined into a single total catchment. It is always desirable to include too much area rather than too little, as additional area that is included within the basin-scale model will continue to drain in its natural direction and will not affect the model results, while missing area will result in an error in the model results.

The following sub-sections describe some common methods by which catchment delineation can be undertaken. This is not an exhaustive list and if other reliable sources or methods for catchment delineation are available to the modeller they should certainly be used alongside some or all of the following methods.

### Delineating the Natural Catchment using the FEH

The FEH web service[^1], previously known as the FEH-CD-ROM, can provide catchment descriptors, rainfall depth-duration-frequency (DDF) estimates and catchment boundaries. Catchment boundaries can only be downloaded for purchased catchments.   

In order to delineate a catchment using the FEH web service the following steps should be followed:

- Navigate to the [map tab](https://fehweb.ceh.ac.uk/Map) and select Great Britain/Northern Ireland in the top right
- Locate the downstream location for the model on the map interface
- Right click at the chosen downstream extent and select catchment boundary
- Check that the catchment is as expected (e.g. area seems appropriate)
- Purchase and download the data.

![FEH web service map portal](images/FEHcatchment.PNG)

This will provide a zipped downloaded folder within which there will be a FEH catchment shapefile titled `FEH_Catchment_X_Y.shp`, where X and Y are the coordinates of the catchment outlet.

We would recommend that this catchment shapefile is saved as `processing\FEH-catchment.shp`
 
### Delineating the Natural Catchment using GRASS

Catchments can be delineated using GRASS tools within QGIS. The input required for this GRASS analysis are LiDAR (Digital Terrain Model) DTM tiles, there are required in Virtual Dataset (vrt) format (if there are multiple LiDAR tiles for the catchment). There is a processing tool to make .vrt's in QGIS that is located under Raster -> Miscellaneous -> Build Virtual Raster. We recommend that a coarser LiDAR resolution is used to enable an easier selection of the outlet point.   

The following GRASS tools are required to delineate the catchment in QGIS:   

#### r.watershed tool

In order to run the r.watershed tool (which can be searched for within the processing toolbox panel), the following steps should be taken:

- LiDAR must be selected for the 'Elevation' parameter (either one LiDAR tile or a .vrt containing all the tiles covering the catchment extent)
- The 'minimum size of exterior watershed basin' should be set to something small (we suggest 30) 
- The 'maximum length of surface flow for USLE' should be set to something large (we recommend 100000)
- Defaults can be used for the remaining parameters, however we would recommend changing the save locations for 'Drainage Direction' and 'Stream Segment' to a sensible location with an appropriate name as these outputs will be used in the next steps of the process.
- Ensure that the 'open output file after running algorithm' boxes are checked on all outputs and run r.watershed.

An example of the suggested parameters are shown below ![r.watershed tool parameters](images/rwatershed.png)

The two key outputs from this tool are 'Drainage Direction' and 'Stream Segment' as these are required to run the r.water.outlet tool, examples are shown below.

![Drainage Direction](images/ddirection.png)
![Stream Segment](images/ssegment.png)

#### r.water.outlet tool
In order to run the r.water.outlet tool (which can be searched for within the processing toolbox panel), the following steps should be taken:

- The name of input raster map should be directed to the 'Drainage Direction' output  
- The coordinates of outlet point (x,y) should be set to the chosen downstream extent of the catchment. The three dots to the right provide the option to select the location on the map in QGIS or the coordinates can be typed however this point must be on a cell within your 'Stream Segment' that is connected to some neighbouring cells. Therefore using the map is recommended
- The save location for 'Basin' should be updated to a sensible location as this will be the final output
- Defaults can be used for the remaining parameters, ensure that the 'open output file after running algorithm' box is checked and run r.water.outlet.

An example of the parameters required is shown below ![r.water.outlet tool](images/rwateroutlet.png)
This will provide a 'Basin' raster as an output (shown below), however it may take a few attempts to get the catchment extent that is desired. It is expected that using a finer LiDAR resolution will require more attempts.

![Basin](images/basin.png) 

We recommend that the output raster from r.water.outlet is converted to a polygon, this can be in QGIS using the Polygonise tool (Raster -> Conversion -> Polygonise), and then stored as `processed/GRASS-catchment.shp`.

### Delineating Surface Water Drainage Networks

TODO: ↑ GM to write

### Defining Additional Catchment Areas

Identifying areas which might contribute to "cross-catchment" flows, either on the surface or through interflow is difficult and we must generally rely on historical anecdote and our judgement about the likelihood of such flows. If such flows are identified or anticipated, the best approach is frequently to simply delineate the whole of the adjacent catchment with which the flow exchange is anticipated and include it within the model.

### Combining Catchment Areas

Through following the above processes, potentially among others, the modeller should now have several polygon areas in shapefile format, each of which represents an estimate of the catchment area of the model. These must be combined into a single catchment area polygon.

Polygons can be merged in QGIS by copying all the polygons (`FEH-catchment.shp`, `GRASS-catchment.shp` and `cross-catchment.shp`) that are to be merged into the same shapefile (it is recommended that a new shapefile is used to prevent overwriting any of the catchment polygons) and then using the advanced digitising tool 'Merge' (shown below). This will provide one shapefile layer with one merged polygon within it.

![Merge](images/merge.png) 

We recommend that the resulting catchment polygon is stored as `processed/combined-catchment.shp`.

## Buffering the Catchment

Once the catchment area has been delineated using the above processes, it should be buffered; i.e. expanded, in every direction, by some fixed amount. The purpose of this is to account for small errors in the catchment delineation and to ensure that the modelled area has the highest chance possible of including the full catchment. We would recommend buffering the catchment by at least 100 metres, but it may be reasonable to increase this if it is perceived that the accuracy of the catchment delineation is poor or if the resolution of the model is very low.

### Buffering with QGIS

To buffer a polygon within QGIS the buffering tool should be used. This is located under Vector -> Geoprocessing -> Buffer. The tool should be run with the following parameters applied:

- Input layer should be set to the `combined-catchment.shp`
- Distance should be set to the desired buffer distance (we recommend at least 100m)
- Remaining parameters can be left as default, however the save location should be updated.

The parameters are shown below ![Buffer](images/buffer.png) 

We suggest that the resulting buffered catchment is saved as `processed/buffered-catchment.shp`

[^1]: [Flood Estimation Handbook web service](https://fehweb.ceh.ac.uk/) on CEH website. Retrieved 16th September 2022.
