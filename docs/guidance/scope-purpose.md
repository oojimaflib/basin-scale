# What is the Purpose of the Model

## A Model Hypothesis

Hydrological analysis is generally done for a reason. Before modelling can begin on any project it is vital to have an understanding of what the modelling aims to show.   
Before undertaking the construction of a model, a modeller must define:

  * The nature and location of the area of interest (AoI).
  * What the potential hydrological sources are and the nature of the hydrological processes at the AoI.
  * What changes to the area of interest and its upstream catchment must be captured by the model
  * The threshold levels of risk

  >what is meant by the threshold levels of risk?
  
## The Area of Interest

An Area of Interest (AoI) is a location within a catchment which we seek to learn about with our hydrological analysis. The area might be a tightly-defined location, such as a specific, small development site, or it might be a wider area in the case of a flood risk mapping project, for example. Either way, it is important to gain a qualitative understanding of the key hydrological processes and sources that will affect flood risk at the area of interest.

>this is the fist time flood risk is mentioned? might we want to say somewhere sooner that this is the focus?

The simplest case for hydrological modelling in general has a small, well-defined area of interest which can be considered, at the catchment scale, to be a point.

## Level of Detail


## The Baseline Scenario


## Other Scenarios


## Storms and Events


