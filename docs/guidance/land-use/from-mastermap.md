# Generating Land Use Data from OS MasterMap

## OS MasterMap Data

The OS MasterMap data will generally be available in GIS format, either as Shapefiles, GML or via the OS APIs. OS MasterMap data consists of six layers:

 - `TopographicPoint`
 - `TopographicLine`
 - `TopographicArea`
 - `BoundaryLine`
 - `CartographicSymbol`
 - `CartographicText`

Of these layers, it is generally only the `Topographic` layers that are used in land use determinations, and of these it is frequently only the `TopographicArea` layer that needs to be consulted. Each feature in the layers has a number of common fields. The fields that are used to generate land use mapping are the `featureCode` and `descriptiveTerm` fields.

## Mapping OS MasterMap Features to Land Uses

The `featureCode` is an integer between 10016 and 10217 that encodes a broad classification of the features. This classification is too broad to form an effective land use map on its own, but some classes map well to the land use classes we recommend. The table below shows how the `featureCode` categories map to some of the recommended categories.

| OS Feature Code | Description                         | Category             |
|-----------------|-------------------------------------|----------------------|
| 10016-10022     | Building                            | Building             |
| 10023-10028     | Buildings or Structure              | Building             |
| 10029-10034     | Built Environment                   | Use Descriptive Term |
| 10042-10051     | General Feature                     | Use Descriptive Term |
| 10052-10059     | General Surface                     | Use Descriptive Term |
| 10060-10064     | Glasshouse                          | Building             |
| 10065-10071     | Height Control                      | Omit                 |
| 10072-10080     | Historic Interest                   | Omit                 |
| 10082-10092     | Inland Water                        | Water                |
| 10093-10106     | Landform                            | Use Descriptive Term |
| 10107-10111     | Natural Environment                 | Use Descriptive Term |
| 10112-10118     | Network or Polygon Closing Geometry | Omit                 |
| 10119-10124     | Path                                | Use Descriptive Term |
| 10126-10137     | Political or Administrative         | Omit                 |
| 10155-10167     | Rail                                | Rail                 |
| 10168-10177     | Road or Track                       | Hardstanding         |
| 10178-10183     | Roadside                            | Hardstanding         |
| 10184-10195     | Structure                           | Use Descriptive Term |
| 10196-10202     | Terrain and Height                  | Omit                 |
| 10203-10212     | Tidal Water                         | Water                |
| 10213-10217     | Unclassified                        | Use Descriptive Term |

Some feature codes, such as 10065 (Height Control) do not map to topographic features and should be omitted while some are too broad such as 10111 (Natural Environment) and additional information is needed to assign a feature to a land use category. In these cases the `descriptiveTerm` field is used to further classify the features.

The `descriptiveTerm` field is an enumeration that takes one (or more) of 141 different values that can be used to disambiguate features from some feature codes. The table below shows how possible `descriptiveTerm` values map to the recommended land use classes.

| OS Descriptive Term             | Category          |
|---------------------------------|-------------------|
| Agricultural Land               | Field             |
| Aqueduct                        | Water             |
| Archway                         | Omit              |
| Bench Mark                      | Omit              |
| Bottom Of Cliff                 | Omit              |
| Bottom Of Slope                 | Omit              |
| Boulders                        | Rough Hard Ground |
| Boulders (Scattered)            | Rough Hard Ground |
| Boundary Half Mereing           | Omit              |
| Boundary Post Or Stone          | Omit              |
| Bridge                          | Hardstanding      |
| Buffer                          | Omit              |
| Canal                           | Water             |
| Canal Feeder                    | Water             |
| Capstan                         | Omit              |
| Cattle Grid                     | Hardstanding      |
| Cave                            | Omit              |
| Chimney                         | Building          |
| Cliff                           | Rough Hard Ground |
| Collects                        | Omit              |
| Compound                        | Omit              |
| Conduit                         | Omit              |
| Coniferous Trees                | Confier           |
| Coniferous Trees (Scattered)    | Conifer           |
| Conveyor                        | Omit              |
| Coppice Or Osiers               | Dense Vegetation  |
| County                          | Omit              |
| Course Of Heritage              | Omit              |
| Crane                           | Omit              |
| Cross                           | Omit              |
| Culvert                         | Omit              |
| Direction Of Flow               | Omit              |
| Distance Marker                 | Omit              |
| District                        | Omit              |
| Disused Feature                 | Omit              |
| Division                        | Omit              |
| Drain                           | Water             |
| Electoral                       | Omit              |
| Electricity Sub Station         | Hardstanding      |
| Emergency Telephone             | Omit              |
| Flagstaff                       | Omit              |
| Footbridge                      | Hardstanding      |
| Ford                            | Water             |
| Foreshore                       | Bare Ground       |
| Fountain                        | Omit              |
| Gantry                          | Omit              |
| Gas Governor                    | Omit              |
| Groyne                          | Omit              |
| Guide Post                      | Omit              |
| Heath                           | Heath             |
| Inferred Property Closing Link  | Omit              |
| Issues                          | Omit              |
| Landfill                        | Rough Hard Ground |
| Landfill (Inactive)             | Rough Hard Ground |
| Letter Box                      | Omit              |
| Level Crossing                  | Rail              |
| Lighting Gantry                 | Omit              |
| Line Of Mooring Posts           | Hardstanding      |
| Line Of Posts                   | Omit              |
| Lock                            | Water             |
| Lock Gate                       | Water             |
| Marsh                           | Marsh             |
| Marsh Reeds Or Saltmarsh        | Marsh             |
| Mast                            | Omit              |
| Mean High Water (Springs)       | Omit              |
| Mean Low Water (Springs)        | Omit              |
| Mill Leat                       | Water             |
| Mine Leat                       | Water             |
| Mineral Workings                | Rough Hard Ground |
| Mineral Workings (Inactive)     | Rough Hard Ground |
| Mooring Post                    | Omit              |
| Mud                             | Bare Ground       |
| Multi Surface                   | Bare Ground       |
| Narrow Gauge                    | Rail              |
| Nonconiferous Trees             | Deciduous         |
| Nonconiferous Trees (Scattered) | Deciduous         |
| Normal Tidal Limit              | Omit              |
| Orchard                         | Deciduous         |
| Outline                         | Omit              |
| Overhead Construction           | Omit              |
| Parish                          | Omit              |
| Parliamentary                   | Omit              |
| Pole                            | Omit              |
| Polygon Closing Link            | Omit              |
| Positioned Boulder              | Rough Hard Ground |
| Positioned Coniferous Tree      | Conifer           |
| Positioned Nonconiferous Tree   | Deciduous         |
| Post                            | Omit              |
| Public                          | Omit              |
| Public Convenience              | Omit              |
| Public Telephone                | Omit              |
| Pylon                           | Omit              |
| Rail Signal Gantry              | Omit              |
| Reeds                           | Marsh             |
| Reservoir                       | Water             |
| Ridge Or Rock Line              | Rough Hard Ground |
| Road Name Or Classification     | Omit              |
| Road Related Flow               | Omit              |
| Rock                            | Rough Hard Ground |
| Rock (Scattered)                | Rough Hard Ground |
| Rough Grassland                 | Rough Grassland   |
| Saltmarsh                       | Marsh             |
| Sand                            | Bare Ground       |
| Scree                           | Rough Hard Ground |
| Scrub                           | Dense Vegetation  |
| Shingle                         | Bare Ground       |
| Signal                          | Omit              |
| Sinks                           | Omit              |
| Site of Heritage                | Omit              |
| Slag Heap                       | Rough Hard Ground |
| Slag Heap (Inactive)            | Rough Hard Ground |
| Slipway                         | Hardstanding      |
| Slope                           | Rough Hard Ground |
| Sloping Masonry                 | Hardstanding      |
| Sluice                          | Omit              |
| Spoil Heap                      | Rough Hard Ground |
| Spoil Heap (Inactive)           | Rough Hard Ground |
| Spot Height                     | Omit              |
| Spreads                         | Omit              |
| Standard Gauge Track            | Rail              |
| Static Water                    | Water             |
| Step                            | Hardstanding      |
| Structure                       | Building          |
| Swimming Pool                   | Water             |
| Switch                          | Omit              |
| Tank                            | Omit              |
| Telecommunications Mast         | Omit              |
| Top Of Cliff                    | Rough Hard Ground |
| Top Of Slope                    | Rough Hard Ground |
| Track                           | Hardstanding      |
| Traffic Calming                 | Hardstanding      |
| Triangulation Point Or Pillar   | Omit              |
| Tunnel Edge                     | Omit              |
| Unmade Path Alignment           | Omit              |
| Upper Level Of Communication    | Omit              |
| Watercourse                     | Water             |
| Waterfall                       | Water             |
| Waterfall (Vertical)            | Water             |
| Weir                            | Omit              |
| Well                            | Omit              |
| Wind Turbine                    | Omit              |

## Recommended Procedure

We recommend the following procedure for generating land use mapping from the MasterMap `topographicArea` layer in GIS:

1. Make a working copy of the `topographicArea` layer. If the data has been supplied in multiple chunks, these should be combined into a single layer and any duplicate features should be identified and removed.
	
2. Filter the layer using the first expression below to polygons that can be classified as buildings. Save the filtered features as a new layer and then delete them from the working layer.

3. Repeat the process above for each of the following expressions to generate layers for each land use class.

### Buildings

```
"featureCode" <= 10028 OR ("featureCode" > 10059 AND "featureCode" < 10065) OR "descriptiveTerm" ILIKE '%structure%'
```
