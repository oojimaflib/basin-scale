# Rainfall Inputs

## General Comments

It is expected that rainfall inputs to a basin-scale will take one of three forms, listed here from the lowest level of detail to the highest.

  * Catchment-averaged rainfall hyetographs.
  * Interpolated hyetographs at point locations.
  * Gridded rainfall inputs.
  
It is a requirement of the method that the input rainfall is distributed in time; i.e. that the rainfall intensity varies over time. The approach is not generally suitable for events defined only by a rainfall total and a duration, although a standardised rainfall distribution could be used in such cases. It is also assumed that the temporal resolution of the rainfall inputs is relatively high (the times between consecutive values in the rainfall hyetographs are short). In general we would not recommend attempting to undertake basin-scale modelling with rainfall timesteps of more than 1 hour, and in general we would recommend timesteps of 15 minutes or less.

This guidance assumes that the available rainfall hyetographs are in the form of accumulations; i.e. the value at each point on the time series corresponds to the amount of rainfall between the previous value's time and the current value's time.
  
## Catchment-averaged hyetographs

A catchment-averaged hyetograph is a rainfall-time series for the catchment with no spatial component; i.e. it is assumed that the rainfall is constant across the catchment. This is generally the simplest approach to implement in a hydraulic model. This approach is reasonable for very small domains, but becomes an increasingly poor representation of reality in larger domains.

## Interpolated hyetographs

Most commonly, basin-scale models will be constructed using observed rainfall data from a network of gauging stations. The resulting input data is in the form of a set of hyetographs (rainfall-time series) where each series is associated with a point location in or near the domain (the location of the gauge). The rainfall inputs for locations between the gauges are then calculated by interpolation. Most commonly, the interpolation is done using inverse distance weighting, but it is not uncommon to simply use a nearest-neighbour approach.

A fuller discussion of interpolation methods for point rainfall hyetographs can be found [here](../science/rainfall-interpolation.md).

## Gridded rainfall inputs

Gridded rainfall inputs can be derived, for large catchments, from the NASA precipitation missions or, on a more local scale, from rainfall radar data sources. The resulting data is in the form of a time series of rainfall accumulation grids which may be directly applied to the model. This is the preferred approach for very large catchments when using the NASA rainfall data and for any catchment when local radar rainfall data is available.


