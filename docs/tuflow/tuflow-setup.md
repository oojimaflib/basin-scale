# Model set-up #

## Control Files 
TUFLOW requires a range of control files in order to build all the necessarily components of the model. These control files are outlined in detail in the TUFLOW Manual[^1]. 

The three key files that are a minimum requirement for TUFLOW models are outlined below, along with their purpose, namely the TUFLOW Control File (TCF), TUFLOW Geometry Control file (TGC) and TUFLOW Boundary Control file (TBC). 


### Tuflow Control File 
The TCF sets the majority of the simulation parameters as well as referencing the TGC and TBC. Every TUFLOW model requires a TCF and we recommend that for a basin-scale model the following commands are used at a minimum:

- Solution Scheme == (HPC/Classic)
- Hardware == (GPU/CPU)
- BC Event Source == (defines events)
- Start Time == (simulation start time)
- End Time == (simulation end time)
- Timestep == (2D timestep)
- Cell Wet/Dry Depth == (required for rainfall models due to substantial amount of shallow sheet flow)
- Read Materials File == (.tmf file)
- BC Database == (boundary condition database in .csv format)
- Geometry Control File == (.tgc file)
- BC Control File == (.tbc file)
- Read Soils File == (.tsoilf file)
- Map Output Format == (DAT/FLT/XMDF)
- Map Output Data Types == (CI dGW IR RFC RFR needed for rainfall models)
- Read GIS PO == (measurement locations)

### Tuflow Geometry Control 
The TGC defines the 2D domain and provides TUFLOW with information on the size of the grid, cell codes, ground elevation, material roughness and soil type. It is important to note that the TGC commands are applied in order, therefore it is possible to overwrite previous information using a subsequent command. This is useful, for example in defining DTM adjustments over LiDAR however care should be taken to apply the commands in the TGC in the order that they are desired to occur. We would recommend that the following commands are applied at minimum:

- Origin == (origin of model)
- Cell Size == (grid cell size)
- Grid Size (X,Y) == (grid size of model)
- Read Grid Zpts == (to read LiDAR tiles)
- Read GIS Code == (sets the model extent)
- Read GIS Mat == (provides material id's)
- Read GIS Soil == (provides soil id's)

### Tuflow Boundary Condition 
The TBC contains information regarding the location and the type of boundaries within the model including the hydrological inputs and outputs. For example; rainfall, inflows to channels and boundaries that allow water to drain appropriately from the model. We would recommend that for a basin scale model the following commands are applied:

- Read GIS RF == (rainfall boundary)
- Read GIS BC == (inflow and outfall boundaries)

Additional control files that could be used for basin-scale model builds in TUFLOW are; 

- Quadtree Control File (QCF) 
- Rainfall Control File (TRFC)

These are outlined within the [Special Topics](build-special-topics.md) and [Rainfall inputs](build-rainfall.md) sections respectively. 

## Data input files

TUFLOW requires data input files, referenced within the control files, in shapefile, csv and text file format. For example, soils files (TSOILF) and materials files (TMF) provide soils parameters and materials files provide roughness values. Information on how to generate the required input files for TUFLOW are provided in the Building the Model section.


 [^1]:[TUFLOW Manual](https://downloads.tuflow.com/_archive/TUFLOW/Releases/2018-03/TUFLOW%20Manual.2018-03.pdf) 2018-03 release.  Retrieved 21st September 2022.