### Outline
TUFLOW[^1] stands for Two-Dimensional Unsteady Flow and is a computer simulation software that is internationally recognised for hydraulic modelling, and has been developed for the past 30 years. The software has advanced 1D/2D and 3D capabilities, with two 2D grid solvers known as Classic and HPC. Both grid solvers can incorporate the 1D solver (ESTRY) if desired, however this guidance focuses on the 2D domain.

TUFLOW uses GIS software in order to build models and is integrated into multiple GIS software packages. TUFLOW models consist of a range of control files, which are outlined in the [Model set-up](tuflow-setup.md) section, and shapefiles which are described in detail in the [Building the Model](build-domain.md) section. This guidance will discuss the TUFLOW build process using the QGIS software package. 


### Applications
The applications of TUFLOW are extensive and the software can be used for the simulation of; flooding, sediment transport, particle tracking, coastal hydraulics and urban drainage. This guidance focuses on the application of TUFLOW software for the simulation of flooding.


### Products

We would recommend that HPC is for basin scale modelling due to it's compatibility with the latest modules and will be used for future updates and functionalities. 

There are a range of modules which can be added to TUFLOW, the following products can be utilised to enhance flood modelling at the basin-scale: 

#### Graphical Processing Units
Graphical Processing Units (GPU) allow the acceleration of hydraulic models by up to 10-400 times compared to computational processing units (CPU), this significantly reduces run times. 

#### Quadtree 
Quadtree is a mesh that is constructed by equally dividing standard cells into four smaller square cells which can be further subdivided if desired. This allows the use of larger cells in areas of less topographic change/interest and smaller cells where terrain varies rapidly/at areas of interest such as primary flow paths. This also has benefits in terms of reduction to computational time.

#### Sub-grid Sampling
Sub-grid Sampling (SGS) allows the modeller to take advantage of sub-grid scale topography that may be present in the DTM but is not utilised within the hydraulic model due to the typical TUFLOW grid cell size. This allows a more accurate representation of the storage and conveyance within the system modelled.

The guidance on how to apply products within TUFLOW are outlined in [Model set-up](tuflow-setup.md) and [Special Topics](build-special-topics.md).

[^1]:[TUFLOW](https://www.tuflow.com/) website. Retrieved 21st September 2022.