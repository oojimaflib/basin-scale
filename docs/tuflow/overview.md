# Overview

The following sections lay out a step-by-step guide for constructing a basin-scale model using the commercial TUFLOW software package.

## General Topics for TUFLOW

Before building a basin-scale model with TUFLOW it is important to understand how the software works and what its strengths and limitations are.

  * [What is TUFLOW?](tuflow-whatis.md)
 * [Model set-up](tuflow-setup.md)

## Building the Model

The following sections provide step-by-step guides for configuring each aspect of a basin-scale model using TUFLOW:

  * [Defining the domain](build-domain.md)
  * [Rainfall inputs](build-rainfall.md)
  * [Terrain model](build-dtm.md)
  * [Measurement locations](build-measurement.md)
  * [Land use mapping](build-landuse.md)
  * [Soils mapping](build-soils.md)
  * [Boundary conditions](build-boundaries.md)
  * [Initial conditions](build-ics.md)
  * [Special topics](build-special-topics.md)

## Calibrating the Model

The following sections describe in detail how to extract calibration outputs from the TUFLOW model and provide approaches for calibrating TUFLOW's parameters in a standardised manner.

  * [Comparing discharge](calibrating-discharge.md)
  * [Comparing percentage run-off](calibrating-pr.md)
  * [Calibrating roughness parameters](calibrating-roughness.md)
  * [Calibrating soil parameters](calibrating-soils.md)
  
## Using the Model

The following sections provide specific guidance for running basin-scale models in TUFLOW and modelling common catchment changes.

  * [Sensitivity analysis](modelling-sensitivity.md)
  * [Modelling topographic change](modelling-z-change.md)
  * [Modelling land-use change](modelling-land-use-change.md)
  * [Modelling climate change](modelling-climate-change.md)

> section on viewing and interpreting results?

