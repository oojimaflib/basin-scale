## Overview

Outlined below are a range of topics which do not fit within the other sections, but nonetheless we thought are worth mentioning.


## Quadtree
Quadtree is a product that TUFLOW offers which provides more control over the 2D mesh as outlined in [What is TUFLOW](tuflow-whatis.md). 

In order to apply Quadtree to a TUFLOW model a Quadtree Control File (QCF) is required. This should contain the following commands:

- Orientation Angle == (optimise) - optimises the grid orientation
- Base Cell Size == (TGC) - sets base cell size to those specified in TGC
- Model Origin and Extent == (TGC) - uses the model origin and extent specified in the TGC
- Read GIS Nesting == (2d_qpc) - quadtree nesting polygons
- Quadtree Mesh Processing Method == (Fast) - use fast processing method


The QCF should be referenced within the TCF, using 'Quadtree Control File ==' and the model must be HPC and it is recommended that GPU is used, as reducing the size of cells increases computational time.

The 2d\_qpc only has one attribute required, within the shapefile, known as the 'Nest level'. The larger the nest level value the smaller the mesh refinement. 

More detail on how to complete a Quadtree build is outlined within [Module 7] of the TUFLOW wiki (https://wiki.tuflow.com/index.php?title=Tutorial_M07) as the TUFLOW Manual currently predates the Quadtree functionality.

## SGS
Sub-Grid-Sampling (SGS) can also be applied to a basin-scale model and is further discussed in [What is TUFLOW](tuflow-whatis.md). To apply SGS to a model no additional control files are required but the following commands need to be applied to existing control files:

- SGS == (ON) - applied within the TCF
- SGS Sample Distance == (number in m) - samples the underlying DTM to provide sub-grid cell topography, applied within the TGC

It is important to note that SGS does not work well in conjunction with GULLY's as there may be overestimation/underestimation of the width of the flow path. SGS can be turned off for specific shapefiles within the TGC using 'SGS OFF' after the 'Read GIS Z Line/Shape' command. This is further discussed on the [TUFLOW wiki](https://wiki.tuflow.com/index.php?title=Quadtree_and_Sub-Grid_Sampling_FAQ).

In addition to this use of SGS increases run times by 20-30% therefore it is recommended to use GPU where possible.

## & Others TBC