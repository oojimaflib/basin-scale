## Overview

Basin-scale models require an outfall in order to allow the water to drain out of the catchment. Ponding at the edges of the model should be avoided by the application of appropriate boundaries at the perimeter of the model. There are multiple ways to do this in TUFLOW (as shown in the Manual[^1]) however, we would recommend using Head-Time boundaries using the following command in the TBC:

- Read GIS BC == (name of HT boundary shapefile)

In addition to this the value that the HT is set to must be referenced within the BC Database.

## Head-Time Boundaries

In order to set Head-Time (HT) boundaries at the perimeter of a basin-scale model it is best to run a version of the model with rainfall applied in order to assess the main flow routes and where water is likely to pond. A new version of the model should then be created with the HT boundary/boundaries applied to any locations where ponding occurred. A HT must be applied using a '2d_bc_L' shapefile with the command 'Read GIS BC ==' in the TBC, as shown below:

![TBC example](images/httbc.png)

We recommend that the shapefile is saved as `model/gis/2d_bc_name_v01-A_L.shp` and a polyline is drawn snapped to the '2d_code'. The attributes should be set to 'HT' for 'Type' with 'Name' referenced within the BC Database. We recommend that this value is set to a little below the LiDAR level, in mAOD, in order to allow the water to drain effectively from the model. An example of the '2d_bc' and the corresponding BC Database is shown below.


![2d_bc example](images/HT.png)

![BC Database](images/HTdbase.png)


[^1]: [TUFLOW Manual](https://downloads.tuflow.com/_archive/TUFLOW/Releases/2018-03/TUFLOW%20Manual.2018-03.pdf)  2018-03 release. Retrieved 29th September 2022.