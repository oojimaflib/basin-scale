## Overview

As outlined in section [Terrain model](../guidance/build-dtm.md) the basin-scale modelling approach requires high-quality representation of the ground surface and the main source of data will be a DTM (most likely derived from LiDAR). TUFLOW permits the correction of DTM grids, therefore we recommend that any adjustments to the DTM are made using the TUFLOW software and the input DTM grids are left unchanged. 

There are several ways to made amendments to a DTM and the method applied should be dependent on the effect that is desired, these methods are outlined in the TUFLOW Manual[^1].

We recommend that the base terrain is defined by the following command within the TGC:

- Set Zpts == (sets all elevations to a specific value within the domain in m)
- Read Grid Zpts == (to read LiDAR tiles)

We recommend that DTM adjustments are defined by the following commands within the TGC (if these are deemed necessary after review of the DTM):

- Read TIN Zpts == (e.g. detailed channel data)  
- Read GIS Z Shape/Line == (e.g. to apply DTM refinements)  

It is important that the base DTM tiles are read in before any DTM adjustments are applied, otherwise these adjustments will be overwritten by the LiDAR.

## DTM

### LiDAR
LiDAR should be sourced for the full extent of your catchment, if GRASS tools were used to delineate the catchment then you will be able to use these tiles for the terrain. TUFLOW currently permits the use of .asc or .flt files most LiDAR is available in one of these formats. LiDAR tiles should be stored somewhere appropriate such as `model/gis/terrain`.  

The 'Set Zpts ==' command should be read in before any LiDAR tiles, we recommend setting the zpts to an appropriate value (999 for example) to ensure that incorrect elevations can be identified easily but the number is low enough so that the model will initialise.

For every LiDAR tile the 'Read Grid Zpts ==' command is required.   

### Alternative DTMs

Alternatives to LiDAR that can be utilised in areas not covered within the catchment can be supplemented using photogrammetry. This ensures that there are no gaps in the ground surface. The same command 'Read Grid Zpts ==' can be used. If the format of the supplied raster is not .asc or .flt then the raster can be converted in QGIS to a format recognised by TUFLOW. This conversion tool within QGIS is located under Raster -> Conversion -> Translate.

## DTM Adjustments

### Detailed Channel modelling
As outlined [earlier](../guidance/build-dtm.md) there may be large watercourses within the catchment that are critical to the hydrological analysis and therefore detailed channel representation is recommended. If sufficient topographic survey data can be obtained then a 3D model, most commonly a Triangular Irregular Network (TIN), can be generated and applied within TUFLOW. In order to apply a TIN within TUFLOW the following command is required 'Read TIN Zpts ==', this needs to be in .tin format.


### Further refinements
If the DTM has been reviewed and it is found that there are some features which are not sufficiently represented in the base DTM, refinements can be made using one of the various TUFLOW commands using shapefiles. There are many options and the type of feature that needs representing will determine the best representation within TULFOW. Therefore we recommend thorough consultation of the TULFOW Manual[^1] in order to inform this decision.

It is likely that the 'Read GIS Z' command will need to be used in conjunction with either 'Shape' or 'Line' depending on whether the adjustment is over a region (and therefore a polygon, requiring the 'Shape' option) or a linear feature (requiring the 'Line' option).  

## TGC Example

The TGC file so far should look similar to the example provided below, remaining commands required for a basin-scale model build are outlined in [Land use Mapping](../tuflow/build-landuse.md) and [Soils Mapping](../tuflow/build-soils.md). ![DTM TGC](images/dtmtgc.png)



[^1]: [TUFLOW Manual](https://downloads.tuflow.com/_archive/TUFLOW/Releases/2018-03/TUFLOW%20Manual.2018-03.pdf)  2018-03 release. Retrieved 23rd September 2022.