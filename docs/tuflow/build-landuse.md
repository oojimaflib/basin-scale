## Overview

As outlined in section [Land use mapping](../guidance/build-landuse.md) the basin-scale modelling approach requires land use mapping to define roughness across the catchment. TUFLOW defines the locations and material codes for different types of land use using a shapefile layer. These material codes correspond to values within a TUFLOW materials file (TMF). The TMF is referenced within the TCF using the command 'Read Materials File =='. 

There are two sources of data for roughness: OS Open Mapping (freely available) and Mastermap (with costs associated based on size of catchment). Mastermap data is more detailed and comprehensive, therefore if the landuse for the catchment is particularly important it may be beneficial to purchase the mastermap data for the catchment. 

Both data types will be read in using the following commands within the TGC:

- Set Mat == (sets default material across domain)
- Read GIS Mat == (provides material codes)


## Material Codes
Material codes are defined using a 2d_mat shapefile which is read in using the 'Read GIS Mat ==' command. In a similar fashion to representing the DTM, it is good practise to set a default material (using 'Set Mat ==') for the full model extent and read in refinements afterwards. This ensures that there are no areas undefined by roughness characteristics in the catchment. 

A 2d_mat shapefile is provided as an empty shapefile by TUFLOW. We recommend that this is stored as `model/gis/2d_mat_name_v01-A_R.shp` and the OS map polygons within the catchment of interest are pasted into it with an appropriate 'ID' applied for each type of land use. 

Care should be taken to ensure that the multiple land use shapefile layers are read into the TGC in the order they are intended to be applied. For example, if there is extensive woodland covering the catchment and the woodland materials layer is read in after the watercourse materials layer the woodland will overwrite the watercourses where they overlap.

The only attribute required is a material 'ID' that corresponds to the first value in the .tmf which allows TUFLOW to give roughness parameters to each 'ID' classification of land use. If mastermap data is used then a shapefile is provided with the appropriate material ID's, this can be read into the TGC directly using the same command with a '| n' after the shapefile in order to read the nth column of data only. 

An example of masetrmap land use cover in 2d_mat shapefile format is shown below ![Material layers](images/mat.png)

## TMF
The .tmf should look similar to the example below, however the codes and roughness values applied will be dependent on the types of land use that are within the catchment. TUFLOW requires the following parameters, these are outlined fully in the TUFLOW Manual[^1]:

- ID (material id corresponding to value in GIS Mat)
- N (manning's 'n' roughness values) 
- Initial loss (initial loss depth from rainfall before application to 2D cells)
- Continuing loss (continuing loss depth from rainfall e.g. useful for interception)
- D1 (depth below which n1 is applied)
- N1 (manning's value applied below d1)
- D2 (depth below which n2 is applied)
- N2 (manning's value applied below d2)
- -1 (reserved for future release but should be set to -1 always)
- SRF (storage reduction factor, default is 0)
- Impervious (fraction impervious value set to 0 or 1)

![Example TMF](images/tmf.png)

We would recommend that depth varying roughness values are applied as this is more representative than one roughness value for some types of land use cover.

## Example TGC

The resultant TGC file so far should look similar to the example provided below, remaining commands required for a basin-scale model build are outlined in [Soils Mapping](../tuflow/build-soils.md). ![Land use TGC](images/landusetgc.png)


[^1]: [TUFLOW Manual](https://downloads.tuflow.com/_archive/TUFLOW/Releases/2018-03/TUFLOW%20Manual.2018-03.pdf)  2018-03 release. Retrieved 23rd September 2022.

## Example TCF
Once land use has been applied the TCF should now look similar to the below. Remaining commands required for a basin-scale model to complete the TCF are found in sections [Soils mapping](build-soils.md) and [Initial Conditions](build-ics.md).

![Example TCF](images/mattcf.png)

