## Overview
As outlined [here](../guidance/build-rainfall.md) there are multiple ways to represent rainfall at the basin-scale, TUFLOW supports all three methods. All methods require the use of a BC Database and the rainfall data must be referenced within a TBC/TRFCF/TCF depending on the method used.

The BC Database and TRFCF/TBC should be referenced within the TCF using the following commands:

- BC Database == (name of BC Database)
- BC Control File == (name of TBC) / Rainfall Control File (name of TRFCF)

The rainfall inputs should be referenced within a shapefile, or within a grid, to give TUFLOW the spatial location of the rainfall data. We recommend that one of the following commands should be used within the TBC/TCF, depending on the rainfall method applied:

- Global Rainfall BC == (catchment-averaged hyetographs)
- Read GIS RF Points == (interpolated hydrographs)
- Read GRID RF == (gridded rainfall inputs)


## Catchment-averaged hyetographs
Catchment-averaged hyetographs are a rainfall-time series for the catchment that has no spatial variation. In order to apply catchment-averaged hyetographs we would recommend the use of the Global Rainfall BC command as this removes the need to generate a '2d_rf' shapefile. The Global Rainfall BC command should be set to the 'Name' that is referenced within the BC Database. This command should be applied within the TBC and an example is shown below:

![Catchment-averaged hyetograph TBC](images/cahtbc.png)

An alternative to the Global Rainfall BC approach is the application of a '2d_rf' as a polygon covering the full catchment. This should have the attributes 'Name' and f1 and f2 should be set to 1 according to the TUFLOW Manual[^1]. If this approach is used the '2d_code' can be copied into an empty '2d_rf' shapefile. This should be stored as `model/gis/2d_rf_name_v01-A_R.shp`.

The BC Database for a catchment-averaged hyetograph should look similar to the below. The value is set to '~AEP~' as there are multiple events that can be called using the 'BC Event Source == ~AEP~ | <<~e1~>>' command within the TCF and defining a TUFLOW event when running the model:

![bc_dbase example](images/bcdbase.png)

The first four columns of the BC Database should be filled in with:

- 'Name' referencing the Global Rainfall BC input
- 'Source' containing the rainfall data in CSV format (example provided below)
- 'Time' 
- 'Value' 

'Value' should correspond to the column of rainfall for the event desired that is within the CSV. 

![Design rainfall data example](images/csv.png)

## Interpolated hyetographs
Interpolated hyetographs usually consist of observed rainfall from a network of gauging stations. TUFLOW provides the option to distribute rainfall across the catchment using three different methods which generate rainfall grids based on point rainfall gauges:

- Inverse Distance Weighting (IDW)
- Polygons 
- TIN (triangulation)

We recommend the use of the IDW approach as this will distribute rainfall across the catchment based on proximity to gauges. To apply interpolated hyetographs, the point locations of the gauges need to applied within a '2d_rf_P' and the 'Read GIS RF Points ==' command should be used within a TRFCF. 

A TRFCF is a rainfall control file which defines to TUFLOW how the interpolation between the rain gauges applied will be calculated. We recommend that the following commands are used and an example is provided below:

- RF Grid Cell Size == (10 times 2D domain is recommended)
- RF Grid Format == (ASC/FLT/NC)
- Read GIS RF Points == (2d_rf_P shapefile)
- RF Interpolation Method == (IDW/POLYGON/TIN)
- IDW Exponent == (2 is default)
- Rainfall Null Value == (recommended for the IDW approach)
- Maximum Hyetograph Points == (controls temporary memory allocation suggested value of 2000)

![TRFCF example](images/trfcf.png)

We recommend that the '2d_rf' is stored as `model/gis/2d_rf_name_v01-A_P.shp`. The attributes required include the 'Name' of the gauge as well as setting f1 and f2 to 1 according to the TUFLOW Manual[^1], an example is provided below. 

![2d_rf](images/2d_rf.png)

The 'Name' should correspond to the 'Name' within the BC Database. In a similar fashion to the catchment-averaged hyetographs the 'Source' should provide the rainfall data in CSV format, however this should contain observed data for a chosen event. The 'Value' should reference a gauged name rather than an AEP. The BC Database and rainfall input data for an interpolated hyetograph should look similar to the below examples:

![BC Database example](images/ihbcdbase.png)

![Observed rainfall data example](images/obscsv.png)


## Gridded rainfall inputs
Gridded rainfall can be sourced from NASA or from radar data, this is provided in time series format which can be applied directly to the model. The command 'Read GRID RF ==' should be applied to the TCF and references the rainfall database, as shown below:

![Rainfall Grid TCF](images/raintcf.png)

 The rainfall database contains two columns 'Time' and 'Rainfall Grid'. The 'Rainfall Grid' column references the rainfall grids which should have the rainfall depth in mm that has fallen over the previous timestep. An example is provided below:

![Rainfall Grid](images/raindbase.png)

## Example TCF
Once one of the methods of rainfall has been applied the TCF should look similar to the below. The example provided is for catchment-averaged hyetographs, remaining commands required for a basin-scale model to complete the TCF are found in sections [Measurement Locations](build-measurement.md), [Land use mapping](build-landuse.md), [Soils mapping](build-soils.md) and [Initial Conditions](build-ics.md).

![Example TCF](images/rainfalltcf.png)

[^1]: [TUFLOW Manual](https://downloads.tuflow.com/_archive/TUFLOW/Releases/2018-03/TUFLOW%20Manual.2018-03.pdf)  2018-03 release. Retrieved 29th September 2022.