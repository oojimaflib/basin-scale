## Overview

TUFLOW does not require measurement locations, however we recommend that these are applied to the model at the areas of interest/where calibration will be undertaken in order to get flow and/or level results in time series format, as outlined [here](../guidance/build-measurement.md).

There are two ways to extract plot outputs in TUFLOW from the floodplain, we recommend that the following command is used and applied to the TCF:

- Read GIS PO == (2D measurement locations)

## Plot Output

TUFLOW can provide plot output (PO) locations and this command can be used multiple times, if desired, for many locations across the catchment. Time series data can be extracted for a range of hydraulic parameters, that must be defined prior to running the simulation.

This can be carried out by generating one or more GIS layers that contain points, polylines or regions defining the location of output. We recommend the use of polylines and the use of an empty QGIS shapefile '2d_po_L.shp'. 

This should be stored as `model/gis/2d_po_name_v01-A.shp` with a line digitised perpendicular to floodplain flow in a downstream direction (from left to right). The type must be set to the plot outputs required, for example '\_Q' should be applied for flow, we would also recommend that a label is written to ensure that the outputs can be found easily within the csv. Other data types that can be extracted are outlined in the TUFLOW Manual[^1].


![2d PO](images/po.png)

## Alternatives

The alternative to a PO line is a reporting location '2d_rl' this provides the flow over the floodplain as well as in the 1D domain. Therefore, this is recommended if a 1D representation of the channel has been applied. The command is as follows, and should also be applied within the TCF:

- Read GIS Reporting Location == (1D and 2D measurement locations)

## TCF Example
The TCF file so far should look similar to the example provide below, remaining commands required for a basin-scale model build are outlined in [Land use mapping](build-landuse.md), [Soils mapping](build-soils.md) and [Initial Conditions](build-ics.md)

![PO TCF](images/potcf.png) 

[^1]: [TUFLOW Manual](https://downloads.tuflow.com/_archive/TUFLOW/Releases/2018-03/TUFLOW%20Manual.2018-03.pdf)  2018-03 release. Retrieved 28th September 2022.