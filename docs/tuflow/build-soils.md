## Overview

As outlined in [Soils mapping](../guidance/build-soils.md) the basin-scale modelling approach requires soils mapping to define infiltration across the catchment. TUFLOW requires a shapefile defining the locations and soil codes for soil types. These soil codes correspond to values within a TUFLOW soils file (TSOILF). The TSOIL is referenced within the TCF using the command 'Read Soils File =='. 

There are two main sources of data for soils that we would recommend: the ISRIC soils dataset (freely available) and the Cranfield soils dataset (with costs associated). Cranfield soils data is more detailed and comprehensive, therefore if the infiltration for the catchment is particularly important it may be beneficial to purchase the Cranfield soils dataset. It should be noted that a Cranfield soils licence only lasts 1 year. 

Both data types will be read in using the following command within the TGC:

- Read GIS Soil == (provides soil codes)


## Soil Codes
Soil codes are defined using a 2d_soil shapefile this is read in with the 'Read GIS Soil ==' command. If the ISRIC soil dataset is used it is recommended that these grids are converted to a shapefile layer using Raster -> Conversion -> Polygonise, pasted into an empty 2d_soil TUFLOW layer and stored as `model/gis/2d_soil_name_v01-A_R.shp`. Alternatively the command 'Read GRID Soil ==' can be used to read the raw grids in .asc or .flt format. For Cranfield soils the data is already provided in shapefile format, we recommend that this is copied and pasted into an empty 2d_soil layer and stored appropriately. 

The only attribute required in a 2d_soil layer is a soil 'ID' that corresponds to the first value in the .tsoilf which allows TUFLOW to give soils parameters to each 'ID' classification of soil. 

An example of a Cranfield 2d_soil shapefile format is shown below ![Soil shapefile](images/soil.png)

## TSOILF
The .toilf should look similar to the example below, however the ID's and soil parameter values applied will be dependent on the types of soil that are within the catchment. 

TUFLOW has three different infiltration methods; Green-Ampt, Horton or Initial/Continuing Losses and requires the following parameters (which are outlined fully in the TUFLOW Manual[^1]):

- ID (soil id)
- GA/HO/ILCL (infiltration method)
- Suction (mm/in)
- Hydraulic conductivity (mm/h or in/h)
- Porosity (fraction of saturated moisture content within the soil)
- Initial moisture (fraction of soil that is initially wet)
- Max ponding depth (in m)

![Example TSOILF](images/tsoilf.png)


## Example TGC

The TGC file should look similar to the example provided below, and will now contain all the minimum commands that we recommend for a basin-scale model build. ![Soils TGC](images/soilstgc.png)

## Example TCF
Once the soils have been applied the TCF should look similar to the below. Remaining commands required for a basin-scale model to complete the TCF are found in the [Initial Conditions](build-ics.md) section.

![Example TCF](images/soiltcf.png)


[^1]: [TUFLOW Manual](https://downloads.tuflow.com/_archive/TUFLOW/Releases/2018-03/TUFLOW%20Manual.2018-03.pdf)  2018-03 release. Retrieved 23rd September 2022.