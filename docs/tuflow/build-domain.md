## Overview

TUFLOW requires the 2D domain to be defined in order to inform the model extent. The domain is defined within the TGC by location, cell size, extent and code. There are several command options that can be used to define the domain in TUFLOW. 

We recommend that the domain is defined by the following commands within the TGC:

- Origin == (location)
- Cell Size == (cell size)
- Grid Size (X,Y) == (extent)
- Set Code == (code)
- Read GIS Code == (code)

## Origin
The Origin for the model needs to be defined in order to locate the model according to a coordinate system in X,Y format. The origin is the most southern corner of the furthest west cell. This point will be informed by the `processed\buffered-catchment.shp` shapefile.

Alternatives to the Origin include 'Read GIS Location', this requires an additional shapefile therefore use of origin is recommended due to simplicity.

## Cell Size
The Cell Size should be applied in metres and should be at least the same grid resolution as the LiDAR DTM but we recommend that the cell size is double the LiDAR resolution to make use of detail within the available data.

However, it should be noted that for modelling at the basin-scale this has significant downsides in terms of computational time. Therefore in order to strike an appropriate balance between run times and using the detail provided within the available data then additional TUFLOW products such as SGS and Quadtree could be considered. Guidance on how to apply these products are provided in the [Special Topics](../tuflow/build-special-topics.md) section.

## Grid Size
The grid size sets the dimensions of the grid in metres along the grid X axis and Y axis. The width and length of the grid size will be determined by the 
`processed\buffered-catchment.shp` shapefile.

Alternatively the 'Read GRID Location ==' command can be used.

## Code

TUFLOW requires a code as each cell in a 2D domain needs to be assigned a code to indicate it's role. Cells can be active or inactive and in order to activate cells (indicates water will be present) a value of '1' is applied and to indicate inactive cells a value of '0' is applied. 

We recommend that the full domain is first set to inactive using 'Set Code == 0' with a shapefile containing the catchment extent read in underneath using 'Read GIS Code' in order to set the active domain.

TUFLOW requires a 2d code layer, which is provided as an empty shapefile as `2d_code_R.shp`. We recommend that a buffered catchment polygon `processed\buffered-catchment` (as outlined in [Defining the Domain](../guidance/build-domain.md)) is pasted into the empty `2d_code_R.shp` TUFLOW shapefile with the 'Code' attribute set to 1. This should be stored as `model\gis\2d_code_name_v01-scenario_R.shp`, with the name of the catchment and scenario applied to that version of the model.

The resulting 2d code should look similar to the image below: ![2d code](images/code.png)

## TGC Example

The TGC file, so far, should look similar to the example provided below. Remaining commands required for a basin-scale model build are outlined in [Terrain Model](../tuflow/build-dtm.md), [Land use Mapping](../tuflow/build-landuse.md) and [Soils Mapping](../tuflow/build-soils.md). 

![Domain TGC](images/domaintgc.png)

## TCF Example

The domain is defined in the TGC and this should be defined in the TCF, an example is shown below. It should be noted that some of the requirements for a basin-scale TUFLOW model, that were mentioned in section [Model set up](tuflow-setup.md), are used. Remaining commands that need to be applied to complete the model build are outlined in sections [Rainfall inputs](build-rainfall.md), [Measurement Locations](build-measurement.md), [Land use mapping](build-landuse.md), [Soils mapping](build-soils.md) and [Initial Conditions](build-ics.md).

![TCF](images/domaintcf.png)
