## Overview

Initial conditions should be applied to a basin-scale model, both in the form of soils (as discussed in [Soils mapping](build-soils.md)) and Initial Water Levels (IWLs) - if there are ponds located within the catchment of interest. This is important due to the implications this can have for calibration as outlined in [Initial Conditions](../guidance/build-ics.md).

In order to represent IWLs for ponds within the catchment, we recommend that a:

- materials layer is used (to represent the pond as impervious), using the command 'Read GIS Mat =='
- z shape layer is used to carve the pond out to be lower than the LiDAR level by a suitable depth (0.5m for example), using the command 'Read GIS Z Shape =='
- and an IWL layer is used to represent the water level at the start of the simulation.

The Read GIS Mat command and the Read GIS Z Shape command are discussed in [Terrain model](build-dtm.md) and [Land use mapping](build-landuse.md) respectively. To represent an IWL we recommend the use of the following command and this should be applied within the TCF:

- Read GIS IWL == (2d_iwl shapefile)


## IWL

To represent an IWL an empty 2d_iwl TUFLOW shapefile should be stored as `model/gis/2d_iwl_name_v01-A_R.shp`. The attribute required is a water level in mAOD, this should be set to the LiDAR level as the water level will be captured by the laser rather than the ground level, an example of a 2d_iwl is shown below. 

![2d_iwl](images/iwl.png)

This shapefile should be referenced within the TCF using the 'Read GIS IWL ==' command, as demonstrated in the example below. This TCF now represents a basin-scale TUFLOW model and contains all the commands we consider necessary to include at minimum.

![Example TCF](images/icstcf.png)