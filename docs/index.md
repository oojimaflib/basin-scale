# Basin-Scale Modelling: A Best-Practice Guide

## What is Basin-Scale Modelling?

Basin-Scale Modelling refers to a technique of hydrological modelling in which:  
 - A two-dimensional hydraulic model is used for the entire catchment upstream of the area of interest
 - The principal hydrological inputs to the model are in the form of rainfall rates distributed across the full model domain
 - The infiltration of the water into the soil is calculated on a temporally- and spatially-varying basis.

 > would it be worth noting topographic input? For me basin-scale modelling has three key inputs: rain, soil, topography. But maybe you were heading in a diff direction here

## Why use Basin-Scale Modelling?

The authors of this guidance believe that the basin-scale modelling approach described here has the ability to provide significantly higher-quality and higher-accuracy estimates of catchments' response to rainfall, under a wider variety of conditions than the hydrological methods that are currently standard in the UK provide. The method is particularly advantageous in catchments which have experienced significant anthropogenic change or for which hydrological estimates for the effects of land-use change in the upper catchment are desired.

> what about permeable catchments? in the conference they were saying how heavily urbanised catchments and permeable catchments can be tricky from a hydrological perspective. If basin-scale modelling is better for permeable catchments as well could be worth a mention?

## Purpose of this documentation

This documentation has three aims:
 - To set out the key considerations that will inform a hydrologist or hydraulic modeller whether a basin-scale approach is appropriate for their problem.
 - To set down what (in the opinion of the authors) represents current best-practice for the schematisation and construction of basin-scale models.
 - To describe, in detail, how to build and operate a basin-scale model using the commercial [TUFLOW](https://tuflow.com) software.
 
## How to use this documentation

There are several ways a hydrologist might wish to use this documentation, depending on their immediate requirements.

A practitioner wishing to fully understand the basin-scale modelling approach would be best served by reading the documentation in the order it is presented, starting with the [high-level overview](high-level/overview.md) and proceeding through the [general guidance](guidance/overview.md), and the [scientific background](science/overview.md) before reaching the [implemetation guidance](tuflow/overview.md) at the end.

Conversely, a modeller wishing to get started quickly could begin with the [general guidance](guidance/overview.md), skipping straight into the relevant implementation details using the links at the end of each section.

Alternatively, it is intended that experienced modellers and practitioners of the method can simply follow the [implementation guidance](tuflow/overview.md), using this document as a check-list of the required steps.

